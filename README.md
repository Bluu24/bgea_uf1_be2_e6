 # **bgea_UF1_Be2_E6**
 
 ## **UF1. Estructures seqüencials, alternatives i iteratives.**
 <br>

## INDEX
- ### [1 Justifica la utilització de les estructures iteratives](#ex-1)

- ### [2 Disseny d’un algorisme que ens demani l’edat 100 vegades](#ex-2)

- ### [3 Disseny d’un algorisme que mostri els 100 primers números naturals](#ex-3)

- ### [4 Disseny d’un algorisme que mostri els nombres parells de l’1 al 100](#ex-4)

- ### [5 Disseny d’un algorisme que mostri els nombres senars de l’1 al 100](#ex-5)

- ### [7 Disseny d’un algorisme que compti de 5 en 5 fins a 100](#ex-7)

- ### [8 Disseny d’un algorisme que vagi del número 100 al número 1 de forma Descendent](#ex-8)

- ### [9 Disseny d’un algorisme que mostri “n” termes de la successió de Fibonacci](#ex-9)

- ### [10 Dissenyeu un algorisme que incrementi el temps en un segon](#ex-10)

- ### [11 Disseny d’un algorisme que calculi les solucions d’una equació de 2n grau](#ex-11)

- ### [12 Donat un text calcular la seva longitud](#ex-12)

- ### [13 Donat un text comptar el nombre de vocals](#ex-13)

- ### [14 Donat un text comptar el nombre de consonants](#ex-14)

- ### [15 Donat un text capgirar-lo](#ex-15)

- ### [16 Donat un text comptar el nombre de paraules que acaben en “ts”](#ex-16)

- ### [17 Donat un text comptar el nombre de paraules](#ex-17)

- ### [18 Donat un text dissenyeu un algorisme que compti els caràcters “as” ](#ex-18)

- ### [19 Dissenyeu un algorisme que comprovi si la paraula és troba dins del text](#ex-19)

- ### [20 Dissenyeu un algorisme que digui quants cops apareix la paraula (parbus) dins del text](#ex-20)

- ### [21 Dissenyeu un algorisme que substitueixi en el text la paraula escollida](#ex-21)


<br>

## EX 1:
 ---
**Justifica la utilització de les estructures iteratives que coneixes i posa’n exemples.**

<div style="color:#4086ff"> Una instrucció iterativa o repetitiva, també coneguda com a bucle , té la missió d'executar les mateixes instruccions de codi una vegada i una altra mentre que es compleixi una determinada condició </div>


## EX 2:
 ---
**Disseny d’un algorisme que ens demani l’edat 100 vegades.**

```c
//#define MAX 100

    //declarant les variables
    int edat ,contador;

    //inicialització
    contador=1;

    while(contador<=MAX){
       //tractat
       printf("Introduex l'edat: ");
       scanf("%i",&edat);
       //obtenir segunet
       contador++;
    }
```
![L’edat 100 vegades](/imatges/2.png)
 
<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>
<br>

## EX 3:
---
**Disseny d’un algorisme que mostri els 100 primers números naturals**
```c
//#define MAX 100

//declarant les variables
    int contador;

    //inicialització
    contador=1;

    while(contador<=MAX){
        //tractar
        printf("%i,",contador);
        //obtenir següent
        contador++;

    }
```

![Els 100 primers números naturals](/imatges/3.png)


<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>
<br>

## EX 4:
---
**Disseny d’un algorisme que mostri els nombres parells de l’1 al 100.**

```c
//#define MAX 100

     //declarant les variables
     int contador;

    //inicialització
    contador=2;

    while(contador<=MAX){
        //tractar
        printf("%i,",contador);
        //obtenir següent
        contador=contador+2;
    }
```
![Nombres parells de l’1 al 100](/imatges/4.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 5:
---
**Disseny d’un algorisme que mostri els nombres senars de l’1 al 100.**

```c
//#define MAX 100

  //declarant les variables
     int contador;

    //inicialització
    contador=1;

    while(contador<=MAX){
        //tractar
        printf("%i,",contador);
        //obtenir següent
        contador=contador+2;
    }

```
![Nombres senars de l’1 al 100](/imatges/5.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 7:
---
**Disseny d’un algorisme que compti de 5 en 5 fins a 100.**

```c
//#define MAX 100

     //declarant les variables
         int contador;

    //inicialització
    contador=5;

    while(contador<=MAX){
        //tractar
        printf("%i,",contador);
        //obtenir següent
        contador=contador+5;
    }
```


![Compti de 5 en 5 fins a 100](/imatges/7.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 8:
---
**Disseny d’un algorisme que vagi del número 100 al número 1 de forma Descendent.**

```c
//#define MIX 1

    //declarant les variables
         int contador;

      //inicialització
      contador=100;

      while(contador>=MIX){
         //tractar
        printf("%i,",contador);
        //obtenir següent
        contador=contador-1;
    }
```
![Número 100 al número 1 de forma Descendent](/imatges/8.png)

## EX 9:
---
**Disseny d’un algorisme que mostri “n” termes de la successió de Fibonacci. Els dos primers termes valen 1 i el terme n-èssim és la suma dels dos anteriors. És a dir, an = an-1 + an-2 .**
```c
//9 declaració de variables
    int contador ,nTermes;
    int n,n1,n2;

    //Inicialització
    contador=3;

    printf("quants termes de la successió de FIBONACCI vols que calculi? ");
    scanf("%i",&nTermes);
    if (nTermes>=1) printf("1, ");
    if (nTermes>=2) printf("1, ");
    if (nTermes>2){
        //inizialització
        n1=1;
        n2=1;

     while(contador<=nTermes){
       //tractar. calcular un terme de la successió de FIBONACCI
         n=n1+n2;
         n2=n1;
         n1=n;

         printf("%i, ",n);
       //obtenir següent
         contador++;
    }
  }

```
![Successió de Fibonacci](/imatges/9.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 10:
---
**A partir de la següent especificació: { anys=ANYS ^ dies=DIES ^ ^hores=HORES ^ minuts= MINUTS ^ segons=SEGONS ^ anys>0 ^ ^0<=dies<365 ^ 0<=hores<24 ^0<=minuts<60 ^ 0<=segons<60 }.**
**Dissenyeu un algorisme que incrementi el temps en un segon.**
```c
 int s,m,h,d,a;
 s=0;
 m=0;
 h=0;
 d=0;
 a=0;

while(1){
   s++;
   if(s==60){
    s=0;
    m++;
    if(m==60){
     m=0;
     h++;
     if(h==24){
      h=0;
      d++;
      if(d==365){
       d=0;
       a++;
      }
     }
    }
   }
 system("cls");
 printf("[A-%.2d D-%.2d H-%.2d M-%.2d S-%.2d]",a,d,h,m,s);
 Sleep(1000);
}
```
![Incrementi el temps en un segon](/imatges/10.png)
<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 11:
---
**Disseny d’un algorisme que calculi les solucions d’una equació de 2n grau.**
```c
(-b +- sqrt ((b*b)-(4*a*c)))
```

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 12:
---
**Donat un text calcular la seva longitud. Recorda que un text, en C, acaba amb el caràcter de final de cadena ‘\0’. A partir d’ara “text” i/o “paraula” ho llegirem com cadena de caràcters o string.**

```c
//Cal declarat el BB = #define BB while(getchar()!='\n')
   //declarar
    int contador;
    char nom [100];

    printf("Introduex un text:");
    scanf("%100[^ \n]",nom);BB;

    //inizalitzar

    contador=0;
    //printf("introdueix un text: ");

    while( nom[contador] !='\0'){

        contador++;

    }
    printf("%i",contador);

```

![Calcular la seva longitud](/imatges/12.png)
<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 13:
---
**Donat un text comptar el nombre de vocals**
```c
//declarar fer amb contans
    int contador;
    char nom [100];
    char vocals[10];
    int numVocals;

    printf("Introduex un text:  ");
    scanf("%100[^ \n]",nom);

    //inizalitzar

    contador=0;
    numVocals=0;

    vocals[0]='a';
    vocals[1]='e';
    vocals[2]='i';
    vocals[3]='o';
    vocals[4]='u';
    vocals[5]='A';
    vocals[6]='E';
    vocals[7]='I';
    vocals[8]='O';
    vocals[9]='U';

    //printf("introdueix un text: ");
    //MENTRE NO FINAL

    while( nom[numVocals] !='\0'){
            //TRACTAR
        if(nom[numVocals]==vocals[0]||
           nom[numVocals]==vocals[1]||
           nom[numVocals]==vocals[2]||
           nom[numVocals]==vocals[3]||
           nom[numVocals]==vocals[4]||
           nom[numVocals]==vocals[5]||
           nom[numVocals]==vocals[6]||
           nom[numVocals]==vocals[7]||
           nom[numVocals]==vocals[8]||
           nom[numVocals]==vocals[9]){
            contador++;
           }
        //OBTENIR SEGUENT
        numVocals++;

    }
    printf("El numero de vocals es: %i",contador);

```

![Numero de Vocals](/imatges/13.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 14:
---
**Donat un text comptar el nombre de consonants**

```c
//declarar fer amb contans
    int contador;
    char nom [100];
    char consonants[21];
    int numConsonants;

    printf("Introduex un text:  ");
    scanf("%100[^ \n]",nom);

    //inizalitzar

    contador=0;
    numConsonants=0;

    consonants[0]='b';
    consonants[1]='c';
    consonants[2]='d';
    consonants[3]='f';
    consonants[4]='g';
    consonants[5]='h';
    consonants[6]='j';
    consonants[7]='k';
    consonants[8]='l';
    consonants[9]='m';
    consonants[10]='n';
    consonants[11]='p';
    consonants[12]='q';
    consonants[13]='r';
    consonants[14]='s';
    consonants[15]='t';
    consonants[16]='v';
    consonants[17]='w';
    consonants[18]='x';
    consonants[19]='y';
    consonants[20]='z';


    //MENTRE NO FINAL

    while( nom[numConsonants] !='\0'){
            //TRACTAR
        if(nom[numConsonants]==consonants[0]||
           nom[numConsonants]==consonants[1]||
           nom[numConsonants]==consonants[2]||
           nom[numConsonants]==consonants[3]||
           nom[numConsonants]==consonants[4]||
           nom[numConsonants]==consonants[5]||
           nom[numConsonants]==consonants[6]||
           nom[numConsonants]==consonants[7]||
           nom[numConsonants]==consonants[8]||
           nom[numConsonants]==consonants[9]||
           nom[numConsonants]==consonants[10]||
           nom[numConsonants]==consonants[11]||
           nom[numConsonants]==consonants[12]||
           nom[numConsonants]==consonants[13]||
           nom[numConsonants]==consonants[14]||
           nom[numConsonants]==consonants[15]||
           nom[numConsonants]==consonants[16]||
           nom[numConsonants]==consonants[17]||
           nom[numConsonants]==consonants[18]||
           nom[numConsonants]==consonants[19]||
           nom[numConsonants]==consonants[20]){
            contador++;
           }
        //OBTENIR SEGUENT
        numConsonants++;

    }
    printf("El numero de Consonants es: %i\n",contador);
```

![Numero de Consonants ](/imatges/14.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 15:
---
**Donat un text capgirar-lo.**
```c
//declarant les variables
    int contador;
    char text[100];
    int index;

    //inicialització
    contador=0;

    printf("Introduex un text:  ");
    scanf("%100[^\n]",text);
    //printf("introdueix un text: ");

    while( text[contador] !='\0'){
        contador++;
    }

    index=contador-1;

    while(text[index]>=0){
        //tractar
        printf("%c",text[index]);
        //obtenir següent
        index=index-1;//index--
    }


```

![text capgirar-lo](/imatges/15.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 16:
---
**Donat un text comptar el nombre de paraules que acaben en “ts”.**
```c
//#define MAX_TEXT 100

 //declarant les variables
        int contadorParaules;
        char text[MAX_TEXT+1];
        int index;

        //inicialització

        printf("Introduex un text: ");
        scanf("%100[^\n]",text);

        index=0;
        while(text[index]==' ') index++;//saltar espai en blanc del principi

        contadorParaules=0;

        while(text[index]!='\0'){
            while(text[index]==' ') index++;//saltar espais en blanc
            while(text[index]!=' ' && text[index]!='\0') index++;//saltar paraules
            if (text[index-1]=='s' && text[index-2]=='t') contadorParaules++;

        }

        printf("El numero de paraules es: %i ",contadorParaules);
```
![Numero de TS](/imatges/16.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 17:
---
**Donat un text comptar el nombre de paraules.**
```c
//#define MAX_TEXT 100

 //declarant les variables
        int contadorParaules;
        char text[MAX_TEXT+1];
        int index;

        //inicialització

        printf("Introduex un text: ");
        scanf("%100[^\n]",text);

        index=0;
        while(text[index]==' ') index++;//saltar espai en blanc del principi

        contadorParaules=0;

        while(text[index]!='\0'){
            while(text[index]==' ') index++;//saltar espais en blanc
            while(text[index]!=' ' && text[index]!='\0') index++;//saltar paraules
            contadorParaules++;
        }

        printf("El numero de paraules es: %i ",contadorParaules);
```
![Comptar el nombre de paraules](/imatges/17.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 18:
---
**Donat un text dissenyeu un algorisme que compti els cops de apareixen conjuntament la parella de caràcters “as” dins del text.** 
```c
 //#define MAX_TEXT 100
 
 //declarant les variables
        int contadorAS;
        char text[MAX_TEXT+1];
        int index;

        //inicialització

        printf("Introduex un text: ");
        scanf("%100[^\n]",text);

        index=0;
        contadorAS=0;

        while(text[index]!='\0'){
            if(text[index]=='a' && text[index+1]=='s'){
                   contadorAS++;
                   }
            index++;

        }
        printf("El numero de AS es: %i ",contadorAS);
```
![Numero de AS](/imatges/18.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 19:
---
**Donat un text i una paraula (anomenada parbus). Dissenyeu un algorisme que comprovi si la paraula és troba dins del text.**

```c
//#define MAX_TEXT 100

//declarant les variables
        char text[MAX_TEXT+1];
        char paraula[100+1];
        char partmp[100+1];
        int index;
        int indexIgual;

        //inicialització

        printf("Introduex un text: ");
        scanf("%100[^\n]",text);BB;

        printf("Introduex una paraula: ");
        scanf("%100[^\n]",paraula);BB;


        index=0;
        while(text[index]!='\0'){
            while(text[index]==' ') index++;//SALTAR_BLANCS
            // **OBTENIR_PARAULA
            indexIgual=0;
            while(text[index]!='\0' && (text[index]!=' ')){
                partmp[indexIgual]=text[index];
                index++;
                indexIgual++;
            }
            partmp[indexIgual]='\0';
            // ***FI OBTENIR PARAULA
            // **iguals(paraula,partmp)?
            indexIgual=0;
            while(paraula[indexIgual]!='\0' && partmp[indexIgual]!='\0'){
                if(paraula[indexIgual] != partmp[indexIgual]) break;
                indexIgual++;
            }
            //paraula[indexIgual]=='\0' || partmp[indexIgual]=='\0'
            //|| paraula[indexIgual] != partmp[indexIgual]

            if(paraula[indexIgual]=='\0' && partmp[indexIgual]=='\0'){
                printf("Paraula Trobada");
                break;
            }
        }
```
<div style="color:#4086ff">TORBAT</div>

![Paraula Trobada](/imatges/19-trovat.png)

<div style="color:#4086ff">NO TROBAT</div>

![Paraula no Trobada](/imatges/19-notrovat.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 20:
---
**Donat un text i una paraula (parbus). Dissenyeu un algorisme que digui quants cops apareix la paraula (parbus) dins del text.**

```c
//#define MAX_TEXT 100

//declarant les variables
        char text[MAX_TEXT+1];
        char paraula[100+1];
        char partmp[100+1];
        int index;
        int indexIgual;
        int comptador;

        //inicialització

        printf("Introduex un text: ");
        scanf("%100[^\n]",text);BB;

        printf("Introduex una paraula: ");
        scanf("%100[^\n]",paraula);BB;


        index=0;
        comptador=0;
        while(text[index]!='\0'){
            while(text[index]==' ') index++;//SALTAR_BLANCS
            // **OBTENIR_PARAULA
            indexIgual=0;
            while(text[index]!='\0' && (text[index]!=' ')){
                partmp[indexIgual]=text[index];
                index++;
                indexIgual++;
                //comptador++;

            }
            partmp[indexIgual]='\0';
            // ***FI OBTENIR PARAULA
            // **iguals(paraula,partmp)?
            indexIgual=0;
            while(paraula[indexIgual]!='\0' && partmp[indexIgual]!='\0'){
                if(paraula[indexIgual] != partmp[indexIgual]) break;
                indexIgual++;

            }
            //paraula[indexIgual]=='\0' || partmp[indexIgual]=='\0'
            //|| paraula[indexIgual] != partmp[indexIgual]
            if(paraula[indexIgual]=='\0' && partmp[indexIgual]=='\0'){
                comptador++;
            }

        }
        printf("La paraula %s a sortit: %i\n",paraula, comptador);
```
![Contar Paraula](/imatges/20.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 21:
---
**Donat un text i una paraula (parbus) i una paraula (parsub).**

**Dissenyeu un algorisme que substitueixi, en el text, totes les vegades que apareix la paraula (parbus) per la paraula (parsub).**

```c
//#define MAX_TEXT 100
//#define MAX_TEXT2 100
//#define BB while (getchar()!='\n')

//declarant les variables
        char text[MAX_TEXT+1];
        char partmp[100+1];
        char paraulaBuS[100+1];
        char paraulaSub[100+1];
        char text2[MAX_TEXT2+1];
        int index;
        int indextmp;
        int index2;
        int indexBus;
        int indexSub;

        //inicialització

        printf("Introduex un text: ");
        scanf("%100[^\n]",text);BB;

        printf("Introduex una paraula que vols trovar: ");
        scanf("%100[^\n]",paraulaBuS);BB;

         printf("Introduex una paraula a Subtituir: ");
        scanf("%100[^\n]",paraulaSub);BB;

        index=0;
        index2=0;
        while(text[index]!='\0'){
            while(text[index]==' '){
                text2[index2]=text[index];
                index++;
                index2++;
            }//SALTAR_BLANCS
            //**OBTENIR_PARAULA
            indextmp=0;
            while(text[index]!='\0' && text[index]!=' '){
                partmp[indextmp]=text[index];
                index++;
                indextmp++;
            }
            partmp[indextmp]='\0';
            //***FI OBTENIR PARAULA
            //**iguals?
            indexBus=0;
            while(partmp[indexBus]!='\0' || paraulaBuS[indexBus]!='\0'){
                if(partmp[indexBus] != paraulaBuS[indexBus]) break;
                indexBus++;

            }
              indexSub=0;
            if(partmp[indexBus]=='\0' && paraulaBuS[indexBus]=='\0'){
                while(paraulaSub[indexSub]!='\0'){
                   text2[index2]=paraulaSub[indexSub];
                   index2++;
                   indexSub++;
                }

              }
                else{
                  while(partmp[indexSub]!='\0'){
                    text2[index2]=partmp[indexSub];
                    index2++;
                    indexSub++;

                }
              }

            text2[index2]='\0';
        }

        printf("\n%s\n",text);
        printf("\n%s\n",text2);
```
![Substituir Paraula](/imatges/21.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>
